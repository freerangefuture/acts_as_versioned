Gem::Specification.new do |s|
  s.specification_version = 2 if s.respond_to? :specification_version=
  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.name              = 'gluttonberg_acts_as_versioned'
  s.version           = '2.0.0'
  s.date              = '2014-02-07'
  s.rubyforge_project = 'gluttonberg_acts_as_versioned'
  s.summary     = "Add simple versioning to ActiveRecord models. This version supports rails 3.2. Also it have support for autosave. It introduces new method auto_save_version which keeps 1 autosave version for an object."
  s.description = "Add simple versioning to ActiveRecord models. rails 3.2"
  s.authors  = ["Nick Crowther", "Yuri Tománek", "Abdul Rauf", "Luke Sutton"]
  s.email    = ["office@freerangefuture.com"]
  s.homepage = 'https://github.com/Gluttonberg/acts_as_versioned'
  s.require_paths = %w[lib]
  s.rdoc_options = ["--charset=UTF-8"]
  s.extra_rdoc_files = %w[README MIT-LICENSE CHANGELOG]
  s.add_dependency('activerecord', ["~> 3.2"])
  s.add_development_dependency 'pg', '~> 0.17', '>= 0.17.1'
  s.files = %w[
    CHANGELOG
    Gemfile
    MIT-LICENSE
    README
    RUNNING_UNIT_TESTS
    Rakefile
    acts_as_versioned.gemspec
    init.rb
    lib/acts_as_versioned.rb
    test/abstract_unit.rb
    test/database.yml
    test/fixtures/authors.yml
    test/fixtures/landmark.rb
    test/fixtures/landmark_versions.yml
    test/fixtures/landmarks.yml
    test/fixtures/locked_pages.yml
    test/fixtures/locked_pages_revisions.yml
    test/fixtures/migrations/1_add_versioned_tables.rb
    test/fixtures/page.rb
    test/fixtures/page_versions.yml
    test/fixtures/pages.yml
    test/fixtures/widget.rb
    test/migration_test.rb
    test/schema.rb
    test/versioned_test.rb
  ]
  s.test_files = s.files.select { |path| path =~ /^test\/test_.*\.rb/ }
end
